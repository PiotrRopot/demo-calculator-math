public class Calculator {
    private int _a;
    private int _b;

    public Calculator() {
    }

    public Calculator(int a, int b) {
        _a = a;
        _b = b;

    }

    public int sum(int a, int b) {
        return a + b;
    }

    public int sub(int a, int b) {
        return a - b;
    }

    public float div(float a, float b) {
        return a / b;
    }

    public int multi(int a, int b) {
        return a * b;
    }

    public int sum() {
        return _a + _b;
    }

    public int sub() {
        return _a - _b;
    }
}
