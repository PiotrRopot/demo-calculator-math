import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testSum() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(2, 2);

        Assert.assertEquals(4, actual);
    }

    @Test
    public void testSub() {
        Calculator calculator = new Calculator();
        int actual = calculator.sub(2, 2);

        Assert.assertEquals(0, actual);
    }

    @Test
    public void testDiv() {
        Calculator calculator = new Calculator();
        float actual = calculator.div(100.0F, 20.0F);

        Assert.assertEquals(5F, actual, 0.0001);
    }

    @Test
    public void testMulti() {
        Calculator calculator = new Calculator();
        int actual = calculator.multi(3, 3);

        Assert.assertEquals(9, actual);
    }

    @Test
    public void testSum2() {
        Calculator calculator = new Calculator(11, 6);
        int actual = calculator.sum();
        Assert.assertEquals(17, actual);

    }

    @Test
    public void testSub2() {
        Calculator calculator = new Calculator(15, 5);
        //int actual = c
        Assert.assertEquals(10, calculator.sub());
    }
}
